""" Entrypoint module """

import argparse
import asyncio
import pickle
import subprocess
import sys
import tempfile
import typing as t
from pathlib import Path

from . import report


class InstrumentError(Exception):
    """Raised when the instrumentation through GDB failed"""

    returncode: int

    def __init__(self, returncode: int, *args, **kwargs):
        self.returncode = returncode
        super().__init__(*args, **kwargs)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--ranges",
        help="Comma-separated list of symbols or PC ranges, eg. 3f33-4a42",
        required=True,
    )
    parser.add_argument(
        "--scope",
        choices=["none", "papi", "perfpipedream"],
        default="none",
        help="Analyze accesses inside instrumentation regions, or globally",
    )
    parser.add_argument("--gdb-quiet", action="store_false", dest="subprocess_stdout")
    parser.add_argument("binary", type=Path, help="Path to the binary to be analyzed")

    args = sys.argv[1:]
    forward_args: list[str] = []
    try:
        delim_pos = args.index("--")
        forward_args = args[delim_pos + 1 :]
        args = args[:delim_pos]
    except ValueError:
        pass

    parsed = parser.parse_args(args)
    return parsed, forward_args


async def run_in_gdb_async(
    binary: t.Union[str, Path],
    pc_ranges: list[t.Union[str, tuple[int, int]]],
    papi_scoping: bool = False,
    perfpipedream_scoping: bool = False,
    gdb_args: t.Optional[list[str]] = None,
    subprocess_stdout: bool = True,
) -> report.Report:
    """Python-level entrypoint, callable as a library function

    binary: path to the binary to instrument
    pc_range: list of either pairs of addresses or symbol names to instrument
    papi_scoping: if False, counts occurrences of basic blocks for the whole execution
        of the program. If True, reset those counts at each PAPI_START, and yield a
        count of occurrences at each PAPI_STOP.
    perfpipedream_scoping: same as papi_scoping, but uses perfpipedream symbols as
        start/stop triggers.
    gdb_args: list of additional arguments to pass to gdb
    subprocess_stdout: if True, gdb will have its stdout redirected to this process'
        stdout. Might help in some cases, or be too verbose in others.
    """

    if isinstance(binary, str):
        binary = Path(binary)

    with tempfile.TemporaryDirectory() as tmpdir_path:
        result_path = Path(tmpdir_path) / "result"
        bootstrap_file = Path(__file__).parent / "bootstrap.py"
        module_name = __name__.rsplit(".", maxsplit=1)[0] + ".instrument"
        python_invoke = f"""{module_name}.entry(
        raw_pc_ranges={repr(pc_ranges)},
        papi_scope={repr(papi_scoping)},
        perfpipedream_scope={repr(perfpipedream_scoping)},
        result_path={repr(result_path.as_posix())},
    )"""

        if gdb_args is None:
            gdb_args = []
        command = (
            [
                "gdb",
            ]
            + gdb_args
            + [
                "--batch-silent",
                "-ex",
                f"source {bootstrap_file.as_posix()}",
                "-ex",
                f"python import {module_name}; {python_invoke}",
            ]
            + [binary.as_posix()]
        )
        process = await asyncio.create_subprocess_exec(
            *command,
            stdout=sys.stdout if subprocess_stdout else subprocess.DEVNULL,
            stderr=sys.stderr,
        )
        await process.wait()
        if process.returncode != 0:
            assert process.returncode is not None
            raise InstrumentError(
                process.returncode,
                f"Instrumentation with gdb returned {process.returncode}",
            )
        with result_path.open("rb") as handle:
            res: report.Report = pickle.load(handle)
            return res


def run_in_gdb(
    binary: t.Union[str, Path],
    pc_ranges: list[t.Union[str, tuple[int, int]]],
    papi_scoping: bool = False,
    perfpipedream_scoping: bool = False,
    gdb_args: t.Optional[list[str]] = None,
    subprocess_stdout: bool = True,
) -> report.Report:
    return asyncio.run(
        run_in_gdb_async(
            binary,
            pc_ranges,
            papi_scoping,
            perfpipedream_scoping,
            gdb_args,
            subprocess_stdout,
        )
    )


def hexrange(elt: str) -> t.Optional[tuple[int, int]]:
    dashsplit = elt.split("-")
    if len(dashsplit) != 2:
        return None
    try:
        return (int(dashsplit[0], 0x10), int(dashsplit[1], 0x10))
    except ValueError:
        return None


def entry():
    """Called from console_scripts"""

    args, gdb_forward = parse_args()
    raw_pc_ranges: list[str] = list(map(lambda x: x.strip(), args.ranges.split(",")))
    pc_ranges: list[t.Union[str, tuple[int, int]]] = []
    for rng in raw_pc_ranges:
        if hex_range := hexrange(rng):
            pc_ranges.append(hex_range)
        else:
            pc_ranges.append(rng)

    try:
        report = run_in_gdb(
            args.binary,
            pc_ranges,
            papi_scoping=(args.scope == "papi"),
            perfpipedream_scoping=(args.scope == "perfpipedream"),
            gdb_args=gdb_forward,
            subprocess_stdout=args.subprocess_stdout,
        )

        for occur_id, occur in enumerate(report.occurrences):
            print(f"\n==== Sample {occur_id+1} ====")
            for blk, count in zip(report.bbs, occur):
                print(f"Block {blk.pc_range[0]:04x}-{blk.pc_range[1]:04x}: {count}")
    except InstrumentError as exn:
        sys.exit(exn.returncode)
