""" Output values from the analysis """

import typing as t


class BBAsm(t.NamedTuple):
    """A basic block analyzed"""

    asm_bytes: bytes
    asm_insns: list[str]


class AnalyzedBB(t.NamedTuple):
    elf_path: str
    pc_range: tuple[int, int]
    with_control: BBAsm
    without_control: BBAsm


class Report(t.NamedTuple):
    """An output value from the analysis"""

    bbs: list[AnalyzedBB]
    occurrences: list[list[int]]
