""" Used to bootstrap Python in gdb. Do not include this file anywhere! It should only
be sourced from a gdb shell. """

import os
import sys
import typing as t
from pathlib import Path


def get_python_path() -> Path:
    def bin_path() -> Path:
        if "VIRTUAL_ENV" in os.environ:
            return Path(os.environ["VIRTUAL_ENV"]) / "bin/python"
        for path_elt in os.environ["PATH"].split(":"):
            python_bin = Path(path_elt) / "python"
            if python_bin.is_file():
                return python_bin
        raise Exception("No python interpreter found")

    python_bin = bin_path()
    python_version = python_bin.resolve().name
    python_prefix = python_bin.parent.parent
    if python_prefix.as_posix() in ["/usr", "/"]:  # System install
        python_prefix = Path.home() / ".local"
    return python_prefix / "lib" / python_version / "site-packages"


def get_egg_links(python_path: Path) -> t.Generator[Path, None, None]:
    """Reads the .egg-link files in the python path and returns a list of paths"""
    for path in python_path.iterdir():
        if path.is_file() and path.suffix == ".egg-link":
            with path.open("r") as handle:
                egg_path = Path(handle.read().split("\n", maxsplit=1)[0].strip())
                if egg_path.exists():
                    yield egg_path.resolve()


python_path = get_python_path()
sys.path.append(python_path.as_posix())
for egg_link in get_egg_links(python_path):
    sys.path.append(egg_link.as_posix())
