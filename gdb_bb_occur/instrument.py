import logging
import pickle
import re
import sys
import typing as t
from collections import defaultdict
from dataclasses import dataclass
from pathlib import Path

from tbastian_pyutils import basic_blocks
from tbastian_pyutils.messages import setup_logging

from .report import AnalyzedBB, BBAsm, Report
from .util import parse_ranges

try:
    import gdb
except ImportError as exn:
    print("This script is intended to be run from insnde GDB.", file=sys.stderr)
    raise exn

logger = logging.getLogger(__name__)


@dataclass
class ProcessInfo:
    text_virt: basic_blocks.Section
    elf_type: str
    elf_path: Path


def parse_gdb_mmap() -> ProcessInfo:
    """Get and parse gdb's memory map."""

    _ELF_INFO_RE = re.compile(
        r"`(?P<elf_path>.*)', file type elf(?:64)?-(?P<arch>[^.]*)."
    )
    _SECTION_RE = re.compile(
        r"0x(?P<start_addr>[0-9a-f]*) - 0x(?P<end_addr>[0-9a-f]*) is (?P<symbol>\.[^ ]*)(?: in (?P<file>.*))?"
    )

    elf_type: t.Optional[str] = None
    elf_path: t.Optional[Path] = None
    text_virt: t.Optional[basic_blocks.Section] = None

    gdb_mmap = gdb.execute("info files", to_string=True)
    for line in gdb_mmap.split("\n"):
        if elfinfo_match := _ELF_INFO_RE.search(line.strip()):
            elf_type = elfinfo_match["arch"]
            elf_path = Path(elfinfo_match["elf_path"])
        elif section_match := _SECTION_RE.match(line.strip()):
            if section_match["symbol"] == ".text" and not section_match["file"]:
                addr = int(section_match["start_addr"], 0x10)
                size = int(section_match["end_addr"], 0x10) - addr
                text_virt = basic_blocks.Section(name=".text", addr=addr, size=size)

    assert elf_type is not None
    assert elf_path is not None
    assert text_virt is not None
    return ProcessInfo(elf_type=elf_type, elf_path=elf_path, text_virt=text_virt)


def get_basic_blocks(
    ranges: list[basic_blocks.AddrRange], bbdec: basic_blocks.BBDecoder
) -> list[basic_blocks.Block]:
    blocks: set[basic_blocks.Block] = set()
    for rng in ranges:
        for sym in bbdec.symbols_in(rng):
            for blk in bbdec.find_blocks(sym):
                blocks.add(blk)
    return list(blocks)


class CountBreakpoint(gdb.Breakpoint):
    address_translation: int = 0
    block: basic_blocks.Block
    occurs: int

    def __init__(self, block: basic_blocks.Block):
        self.block = block
        self.occurs = 0
        super().__init__(f"*0x{block.addr + self.__class__.address_translation:x}")

    def reset_occurs(self):
        self.occurs = 0

    def stop(self) -> bool:
        """Called when the breakpoint is hit"""
        self.occurs += 1
        return False


class AtReport(gdb.Breakpoint):
    REPORT_AT: str
    block_breaks: list[CountBreakpoint]
    report_list: list[list[int]]

    def __init__(
        self, block_breaks: list[CountBreakpoint], report_list: list[list[int]]
    ):
        self.block_breaks = block_breaks
        self.report_list = report_list
        super().__init__(self.__class__.REPORT_AT)

    def stop(self) -> bool:
        occurs = [blk.occurs for blk in self.block_breaks]
        self.report_list.append(occurs)
        for brk in self.block_breaks:
            logger.debug(
                "Block %04x-%04x: %d hits", brk.block.addr, brk.block.end, brk.occurs
            )
        return False


class AtExit(AtReport):
    REPORT_AT = "exit"


class AtPapiStop(AtReport):
    REPORT_AT = "PAPI_stop"


class AtPerfPipedreamStop(AtReport):
    REPORT_AT = "perf_pipedream_stop"


class AtInstrumentStart(gdb.Breakpoint):
    INSTRUMENT_AT: str
    block_breaks: list[CountBreakpoint]

    def __init__(self, block_breaks: list[CountBreakpoint]):
        self.block_breaks = block_breaks
        super().__init__(self.__class__.INSTRUMENT_AT)

    def stop(self) -> bool:
        for brk in self.block_breaks:
            brk.reset_occurs()
        return False


class AtPapiStart(AtInstrumentStart):
    INSTRUMENT_AT = "PAPI_start"


class AtPerfPipedreamStart(AtInstrumentStart):
    INSTRUMENT_AT = "perf_pipedream_start"


def instrument_blocks(
    blocks: list[basic_blocks.Block],
) -> t.Generator[CountBreakpoint, None, None]:
    for blk in blocks:
        yield CountBreakpoint(blk)


def entry(
    raw_pc_ranges: list[t.Union[str, tuple[int, int]]],
    papi_scope: bool,
    perfpipedream_scope: bool,
    result_path: str,
):
    """Entrypoint, executed when sourced from gdb"""

    setup_logging(loglevel=logging.INFO)
    gdb.execute("starti")
    process_info = parse_gdb_mmap()

    decoder = basic_blocks.BBDecoder(process_info.elf_path)
    text_section = decoder.get_section(".text")
    text_phys = text_section.addr
    CountBreakpoint.address_translation = process_info.text_virt.addr - text_phys

    pc_ranges: list[basic_blocks.AddrRange] = list(parse_ranges(raw_pc_ranges, decoder))

    blocks = get_basic_blocks(pc_ranges, decoder)
    blocks.sort()
    breakpoints = list(instrument_blocks(blocks))
    logger.debug("Monitoring %d blocks", len(breakpoints))

    report_bbs: list[AnalyzedBB] = []
    for bb in blocks:
        decoded = decoder.get_block_data(bb, disasm=True)
        decoded_nocontrol = decoded.filter_control_insn()
        assert (
            decoded.disasm is not None and decoded_nocontrol.disasm is not None
        )  # disasm = True
        if (
            decoded.raw and decoded_nocontrol.raw
        ):  # Filter out empty blocks (eg. only control insn)
            elf_path = process_info.elf_path.as_posix()
            report_bbs.append(
                AnalyzedBB(
                    elf_path=elf_path,
                    pc_range=(bb.addr, bb.end),
                    with_control=BBAsm(
                        asm_bytes=decoded.raw,
                        asm_insns=[
                            f"{insn.mnemonic} {insn.op_str}" for insn in decoded.disasm
                        ],
                    ),
                    without_control=BBAsm(
                        asm_bytes=decoded_nocontrol.raw,
                        asm_insns=[
                            f"{insn.mnemonic} {insn.op_str}"
                            for insn in decoded_nocontrol.disasm
                        ],
                    ),
                )
            )
    occurrences: list[list[int]] = []

    if papi_scope:
        AtPapiStart(breakpoints)
        AtPapiStop(breakpoints, occurrences)
    elif perfpipedream_scope:
        AtPerfPipedreamStart(breakpoints)
        AtPerfPipedreamStop(breakpoints, occurrences)
    else:
        AtExit(breakpoints, occurrences)

    gdb.execute("run")

    report = Report(bbs=report_bbs, occurrences=occurrences)
    with open(result_path, "wb") as handle:
        pickle.dump(report, handle)
