""" Util functions that do not require the gdb module """

import logging
import typing as t

from tbastian_pyutils import basic_blocks

logger = logging.getLogger(__name__)


class BadArgument(Exception):
    """Raised when called with bad arguments"""


def coalesce_ranges(
    ranges: list[basic_blocks.AddrRange],
) -> t.Generator[basic_blocks.AddrRange, None, None]:
    """Coalesce intersecting ranges, assuming the input list is sorted. Preserves
    original objects as much as possible."""

    def can_join(lower: basic_blocks.AddrRange, upper: basic_blocks.AddrRange) -> bool:
        """True if blocks intersect or touch"""
        return upper.addr in lower or upper.addr == lower.end

    coalesced_block: t.Optional[basic_blocks.AddrRange] = None
    for pos, cur_blk in enumerate(ranges):
        if coalesced_block is not None:
            if can_join(coalesced_block, cur_blk):
                new_end = max(coalesced_block.end, cur_blk.end)
                if new_end > coalesced_block.end:
                    coalesced_block = basic_blocks.AddrRange(
                        addr=coalesced_block.addr, size=new_end - coalesced_block.addr
                    )
            else:
                yield coalesced_block
                coalesced_block = None

        if coalesced_block is None:
            if pos + 1 < len(ranges) and can_join(cur_blk, ranges[pos + 1]):
                coalesced_block = cur_blk
            else:
                yield cur_blk
    if coalesced_block is not None:
        yield coalesced_block


def parse_ranges(
    ranges: list[t.Union[str, tuple[int, int]]], decoder: basic_blocks.BBDecoder
) -> t.Generator[basic_blocks.AddrRange, None, None]:
    """Parse a comma-separated list of PC ranges, in either of two forms:
    * a symbol name, or
    * a range eg. 3f31-4a5f
    """

    # Parse ranges
    out: list[basic_blocks.AddrRange] = []
    for elt in ranges:
        # Pair of ints -- address range
        if isinstance(elt, tuple):
            block = basic_blocks.AddrRange(elt[0], elt[1] - elt[0])
            try:
                section = decoder.section_at(block.addr)
                if section.name != ".text" or block.end not in section:
                    raise BadArgument(f"PC range {elt} not included in binary's .text")
            except basic_blocks.SectionNotFound as exn:
                raise BadArgument(
                    f"PC range {elt} not included in binary's .text"
                ) from exn

            out.append(block)
        # string -- supposedly a symbol name
        else:
            try:
                sym = decoder.get_symbol(elt)
                out.append(sym)
            except basic_blocks.SymbolNotFound as exn:
                logger.critical(
                    "Cannot parse PC range %s: neither range nor symbol", elt
                )
                raise BadArgument(
                    f"Bad PC range {elt}: not a PC range, and no matching symbol found"
                ) from exn

    out.sort()

    return coalesce_ranges(out)
