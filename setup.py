#!/usr/bin/env python3

from setuptools import setup, find_packages
import sys


def parse_requirements():
    reqs = []
    with open("requirements.txt", "r") as handle:
        for line in handle:
            if line.startswith("-") or line.startswith("git+"):
                continue
            reqs.append(line)
    return reqs


setup(
    name="gdb_bb_occur",
    version="0.1.0",
    description="Count basic blocks occurrences with GDB",
    author="CORSE",
    license="LICENSE",
    packages=find_packages(),
    include_package_data=True,
    package_data={"gdb_bb_occur": ["py.typed"]},
    long_description=open("README.md").read(),
    install_requires=parse_requirements(),
    entry_points={
        "console_scripts": [
            ("gdb-bb-occur = gdb_bb_occur.entrypoint:entry"),
        ]
    },
)
