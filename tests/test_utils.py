from tbastian_pyutils.basic_blocks import AddrRange, NamedRange

from gdb_bb_occur import util


def test_coalesce_ranges():
    # coalesce_ranges assumes that the ranges are sorted according to
    # `AddrRange.__lt__`

    # Simple coalescing
    ranges = [
        AddrRange(0x0, 10),
        AddrRange(0x9, 7),  # Up to 0x10
        AddrRange(0xA, 3),  # Included
        AddrRange(0x20, 2),
        AddrRange(0x21, 2),
    ]
    coalesced = list(util.coalesce_ranges(ranges))
    assert coalesced == [AddrRange(0, 0x10), AddrRange(0x20, 3)]

    # Joining touching blocks
    ranges = [
        AddrRange(0x0, 0x10),
        AddrRange(0x10, 0x10),
    ]
    coalesced = list(util.coalesce_ranges(ranges))
    assert coalesced == [AddrRange(0, 0x20)]

    # Preserve blocks if possible
    b1 = NamedRange(0x0, 0x10, name="b1")
    b2 = NamedRange(0x20, 0x10, name="b2")
    ranges = [b1, b2, AddrRange(0x21, 2)]
    coalesced = list(util.coalesce_ranges(ranges))
    assert coalesced == [b1, b2]
    assert coalesced[0] is b1
    assert coalesced[1] is b2
